<body class="homepage">
  <header id="header" >
    <nav class="navbar navbar-fixed-top" role="banner" style="background-image: url('master/client/images/background - Copy.jpg'); border-bottom: 5px solid #E8CE0E;">
      <div class="container">
          <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./" style="color: #ffffff">Outcore</a>
          </div>

           <!--  <div class="collapse navbar-collapse navbar-right">
              <ul class="nav navbar-nav">
                <li class="active"><a href="./">Home</a></li>
                <li><a href="#">Company</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Career</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
            </div> -->
            <div class="collapse navbar-collapse navbar-right">
                <div class="navigation">
                    <nav>
                      <ul class="nav navbar-nav">
                        <li class="dropdown active">
                          <a href="index.html"><i class="icon-home"></i> Home <i class="icon-angle-down"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#">Company <i class="icon-angle-down"></i></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Filosofi Logo</a></li>
                            <li><a href="#">Promotion Video</a></li>
                            <li><a href="#">Vision Mission</a></li>
                            <li><a href="#">Why Us</a></li>
                            <li><a href="#">Perijinan & Keanggotaan</a></li>
                            <li><a href="#">Sertifikasi</a></li>
                            <li><a href="#">Testimonials</a></li> 
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#">Service <i class="icon-angle-down"></i></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Cleaning Service</a></li>
                            <li><a href="#">Gardening Service</a></li>
                            <li><a href="#">Security Service</a></li>
                            <li><a href="#">Rope Access Service</a></li>
                            <li><a href="#">Business Support Service</a></li>
                            <li><a href="#">Landscape</a></li>
                            <li><a href="#">Restorasi Marmer</a></li>
                            <li><a href="#">Shampooing Carpet</a></li>
                            <li><a href="#">General Cleaning</a></li>
                            <li><a href="#">Salon Toilet</a></li>
                            <li><a href="#">Diklat Satpam</a></li>
                            <li><a href="#">Diklat Klining Servis</a></li>
                            <li><a href="#">Wellhos</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#">News <i class="icon-angle-down"></i></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">News</a></li>
                            <li><a href="#">Photo Gallery</a></li>
                            <li><a href="#">Inspirational Video</a></li>
                            <li><a href="#">Release Media</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#">Career <i class="icon-angle-down"></i></a>
                        </li>
                        <li>
                          <a href="contact.html">Contact </a>
                        </li>
                      </ul>
                    </nav>
                </div>
            </div>
      </div>
      <!--/.container-->
    </nav>
    <!--/nav-->

  </header>
  <!--/header-->
</body>
