 <section id="bottom" style="background-color: #016080 ">
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="widget">
            <h3>Company</h3>
            <ul>
              <li><a href="#">About us</a></li>
              <li><a href="#">We are hiring</a></li>
              <li><a href="#">Meet the team</a></li>
              <li><a href="#">Copyright</a></li>
            </ul>
          </div>
        </div>
        <!--/.col-md-3-->

        <div class="col-md-3 col-sm-6">
          <div class="widget">
            <h3>Support</h3>
            <ul>
              <li><a href="#">Faq</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Forum</a></li>
              <li><a href="#">Documentation</a></li>
            </ul>
          </div>
        </div>
        <!--/.col-md-3-->

        <div class="col-md-3 col-sm-6">
          <div class="widget">
            <h3>Developers</h3>
            <ul>
              <li><a href="#">Web Development</a></li>
              <li><a href="#">SEO Marketing</a></li>
              <li><a href="#">Theme</a></li>
              <li><a href="#">Development</a></li>
            </ul>
          </div>
        </div>
        <!--/.col-md-3-->

        <div class="col-md-3 col-sm-6">
          <div class="widget">
            <h3>Our Partners</h3>
            <ul>
              <li><a href="#">Adipisicing Elit</a></li>
              <li><a href="#">Eiusmod</a></li>
              <li><a href="#">Tempor</a></li>
              <li><a href="#">Veniam</a></li>
            </ul>
          </div>
        </div>
        <!--/.col-md-3-->
      </div>
    </div>
  </section>
<footer id="footer" class="midnight-blue" style="background-image: url('master/client/images/background - Copy.jpg');">
  <div class="container">
    <div class="row">
      <div style="text-align: left;" class="col-sm-6">
        &copy;  Copyright Jemberkita. online
         <div class="credits">
          Designed by <a href="http://jemberkita.online/">Jemberkita</a>
        </div>
      </div>
     
      <div class="col-sm-6">
        <!-- <ul class="pull-right">
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Faq</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul> -->
        
      </div>
    </div>
  </div>
</footer>

<!-- floating button -->
<div class="zoom">
  <a class="zoom-fab-1 zoom-btn-large" id="zoomBtn" style="background-image: url('master/client/images/floating_button/background.jpg');">
    <i class="">
      <img style="width: 85%; color:#ffffff; margin-top: 15%" src="master/client/images/floating_button/call2.png">
    </i>
  </a>
  <ul class="zoom-menu-1">
    <li>
      <a href="mailto:Arif.umcjbr@gmail.com" class="zoom-fab-2 zoom-btn-sm zoom-btn-envelope scale-transition scale-out" style="color: #ffffff">
        <i class="fa fa-envelope" style="margin-top: 10px"></i>
      </a>
    </li>
  </ul>
  <ul class="zoom-menu-2">
    <li>
        <a href="https://api.whatsapp.com/send?phone=628123463120" class="zoom-fab-2 zoom-btn-sm zoom-btn-whatsapp scale-transition scale-out" style="color: #ffffff">
          <i class="fa fa-whatsapp" style="margin-top: 10px"></i>
        </a>
      </li>
  </ul>
</div>
<!--/#footer-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="master/client/js/jquery.js"></script>
<script src="master/client/js/bootstrap.min.js"></script>
<script src="master/client/js/jquery.prettyPhoto.js"></script>
<script src="master/client/js/jquery.isotope.min.js"></script>
<script src="master/client/js/wow.min.js"></script>
<script src="master/client/js/main.js"></script>

</body>
