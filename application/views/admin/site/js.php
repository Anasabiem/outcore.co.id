<script src="<?php echo base_url() ?>master/admin/assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?php echo base_url() ?>master/admin/assets/js/bootstrap.min.js"></script>
	 
    <!-- Metis Menu Js -->
    <script src="<?php echo base_url() ?>master/admin/assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="<?php echo base_url() ?>master/admin/assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url() ?>master/admin/assets/js/morris/morris.js"></script>
	
	
	<script src="<?php echo base_url() ?>master/admin/assets/js/easypiechart.js"></script>
	<script src="<?php echo base_url() ?>master/admin/assets/js/easypiechart-data.js"></script>
	
	
    <!-- Custom Js -->
    <script src="<?php echo base_url() ?>master/admin/assets/js/custom-scripts.js"></script>
