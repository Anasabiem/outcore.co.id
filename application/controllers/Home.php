<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		// $this->load->view('client/Home');
		$data['page']='client/home';
		$this->load->view('client/main',$data);
	}
}
